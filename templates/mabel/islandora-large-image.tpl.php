<?php

/**
* @file
* This is the template file for the object page for large image
*
* Available variables:
* - $islandora_object: The Islandora object rendered in this template file
* - $islandora_dublin_core: The DC datastream object
* - $dc_array: The DC datastream object values as a sanitized array. This
*   includes label, value and class name.
* - $islandora_object_label: The sanitized object label.
* - $parent_collections: An array containing parent collection(s) info.
*   Includes collection object, label, url and rendered link.
* - $islandora_thumbnail_img: A rendered thumbnail image.
* - $islandora_content: A rendered image. By default this is the JPG datastream
*   which is a medium sized image. Alternatively this could be a rendered
*   viewer which displays the JP2 datastream image.
*
* @see template_preprocess_islandora_large_image()
* @see theme_islandora_large_image()
*/
?>
<div class="islandora-large-image-object islandora" vocab="http://schema.org/" prefix="dcterms: http://purl.org/dc/terms/" typeof="ImageObject">

  <div class="block--content-switcher">
    <div class="mabel-content-switcher" role="tablist" aria-label="Title of Content Switcher">
      <button class="switch-1" role="tab" id="mabel-object-switch-button-1" title="mabel-object-switch-button-1" aria-controls="mabel-object-switch-content-1">
        View Object
      </button>

      <button class="switch-2" role="tab" id="mabel-object-switch-button-2" title="mabel-object-switch-button-2" aria-controls="mabel-object-switch-content-2">
        Description
      </button>

      <button class="switch-3" role="tab" id="mabel-object-switch-button-3" title="mabel-object-switch-button-3" aria-controls="mabel-object-switch-content-3">
        Side-by-Side
      </button>
    </div>

    <div class="download-link">
      <?php if ($islandora_content): ?>
        <?php if (isset($image_clip)): ?>
          <?php print $image_clip; ?>
        <?php endif; ?>
      <?php endif; ?>
    </div>

    <div class="mabel-content-switcher-container">

      <div class="switch-1 content" role="tabpanel" id="mabel-object-switch-content-1" aria-labelledby="mabel-object-switch-button-1" tabindex="0">
        <div id="viewer-container">
          <div class="islandora-large-image-content-wrapper islandora-content-wrapper clearfix">
            <?php if ($islandora_content): ?>
              <div class="islandora-large-image-content">
                <?php print $islandora_content; ?>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>

      <div class="switch-2 content" role="tabpanel" id="mabel-object-switch-content-2" aria-labelledby="mabel-object-switch-button-2" tabindex="0">
        <div id="metadata-container">
          <div class="islandora-large-image-metadata islandora-metadata-set">
            <?php print $description; ?>
            <?php if ($parent_collections): ?>
              <fieldset>
                <legend><span class="fieldset-legend"><?php print t('In collections'); ?></span></legend>
                <ul>
                  <?php foreach ($parent_collections as $collection): ?>
                    <li><?php print l($collection->label, "islandora/object/{$collection->id}"); ?></li>
                  <?php endforeach; ?>
                </ul>
              </fieldset>
            <?php endif; ?>
            <?php print $metadata; ?>
          </div>
        </div>
      </div>

      <div class="switch-3 content" role="tabpanel" id="mabel-object-switch-content-3" aria-labelledby="mabel-object-switch-button-3" tabindex="0">
        <div class="layout layout--twocol-section layout--twocol-section--50-50 side-by-side">
          <div  class="layout__region layout__region--first" id="side-by-side-viewer-container">
          </div>

          <div  class="layout__region layout__region--second" id="side-by-side-metadata-container">
          </div>
        </div>
      </div>

    </div>

  </div>
</div>
