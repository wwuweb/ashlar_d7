<?php

/**
* @file
* This is the template file for the object page for basic image
*
* @TODO: add documentation about file and available variables
*/
?>

<div class="islandora-basic-image-object islandora" vocab="http://schema.org/" prefix="dcterms: http://purl.org/dc/terms/" typeof="ImageObject">

  <div class="block--content-switcher">
    <div class="mabel-content-switcher" role="tablist" aria-label="Title of Content Switcher">
      <button class="switch-1" role="tab" id="mabel-object-switch-button-1" title="mabel-object-switch-button-1" aria-controls="mabel-object-switch-content-1">
        View Object
      </button>

      <button class="switch-2" role="tab" id="mabel-object-switch-button-2" title="mabel-object-switch-button-2" aria-controls="mabel-object-switch-content-2">
        Description
      </button>

      <button class="switch-3" role="tab" id="mabel-object-switch-button-3" title="mabel-object-switch-button-3" aria-controls="mabel-object-switch-content-3">
        Side-by-Side
      </button>
    </div>

    <div class="mabel-content-switcher-container">

      <div class="switch-1 content" role="tabpanel" id="mabel-object-switch-content-1" aria-labelledby="mabel-object-switch-button-1" tabindex="0">
        <div id="viewer-container">
          <div class="islandora-basic-image-content-wrapper islandora-content-wrapper clearfix">
            <?php if (isset($islandora_content)): ?>
              <div class="islandora-basic-image-content">
                <?php print $islandora_content; ?>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>

      <div class="switch-2 content" role="tabpanel" id="mabel-object-switch-content-2" aria-labelledby="mabel-object-switch-button-2" tabindex="0">
        <div id="metadata-container">
          <?php print $description; ?>
          <?php if ($parent_collections): ?>
            <fieldset>
              <legend><span class="fieldset-legend"><?php print t('In collections'); ?></span></legend>
              <ul>
                <?php foreach ($parent_collections as $collection): ?>
                  <li><?php print l($collection->label, "islandora/object/{$collection->id}"); ?></li>
                <?php endforeach; ?>
              </ul>
            </fieldset>
          <?php endif; ?>
          <?php print $metadata; ?>
        </div>
      </div>

      <div class="switch-3 content" role="tabpanel" id="mabel-object-switch-content-3" aria-labelledby="mabel-object-switch-button-3" tabindex="0">
        <div class="layout layout--twocol-section layout--twocol-section--50-50 side-by-side">
          <div  class="layout__region layout__region--first" id="side-by-side-viewer-container">
          </div>

          <div  class="layout__region layout__region--second" id="side-by-side-metadata-container">
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
