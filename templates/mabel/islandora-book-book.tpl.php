<?php
/**
* @file
* Template file to style output.
*/
?>

<div class="block--content-switcher">
  <div class="mabel-content-switcher" role="tablist" aria-label="Title of Content Switcher">
    <button class="switch-1" role="tab" id="mabel-object-switch-button-1" title="mabel-object-switch-button-1" aria-controls="mabel-object-switch-content-1">
      View Object
    </button>

    <button class="switch-2" role="tab" id="mabel-object-switch-button-2" title="mabel-object-switch-button-2" aria-controls="mabel-object-switch-content-2">
      Description
    </button>

    <button class="switch-3" role="tab" id="mabel-object-switch-button-3" title="mabel-object-switch-button-3" aria-controls="mabel-object-switch-content-3">
      Side-by-Side
    </button>
  </div>

  <div class="mabel-content-switcher-container">

    <div class="switch-1 content" role="tabpanel" id="mabel-object-switch-content-1" aria-labelledby="mabel-object-switch-button-1" tabindex="0">
      <div id="viewer-container">
        <div class="islandora-book-content-wrapper islandora-content-wrapper clearfix">
          <?php if(isset($viewer)): ?>
            <div id="book-viewer">
              <?php print $viewer; ?>
            </div>
          <?php endif; ?>
        </div>
      </div>

      <div class="switch-2 content" role="tabpanel" id="mabel-object-switch-content-2" aria-labelledby="mabel-object-switch-button-2" tabindex="0">
        <div id="metadata-container">
          <?php if($display_metadata === 1): ?>
            <div class="islandora-book-metadata islandora-metadata-set">
              <?php print $description; ?>
              <?php if($parent_collections): ?>
                <fieldset>
                  <legend class="fieldset-legend"></span><?php print t('In collections'); ?></span></legend>
                  <ul>
                    <?php foreach ($parent_collections as $collection): ?>
                      <li><?php print l($collection->label, "islandora/object/{$collection->id}"); ?></li>
                    <?php endforeach; ?>
                  </ul>
                </fieldset>
              <?php endif; ?>
              <?php print $metadata; ?>
            </div>
          <?php endif; ?>
        </div>
      </div>

      <div class="switch-3 content" role="tabpanel" id="mabel-object-switch-content-3" aria-labelledby="mabel-object-switch-button-3" tabindex="0">
        <div class="layout layout--twocol-section layout--twocol-section--50-50 side-by-side">
          <div  class="layout__region layout__region--first" id="side-by-side-viewer-container">
          </div>

          <div  class="layout__region layout__region--second" id="side-by-side-metadata-container">
          </div>
        </div>
      </div>

    </div>
  </div>
