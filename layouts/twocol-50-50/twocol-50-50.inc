<?php

// Plugin definition
$plugin = array(
  'title' => t('Two Column 50/50'),
  'category' => t('Ashlar'),
  'icon' => 'twocol-50-50.png',
  'theme' => 'twocol-50-50',
  'css' => '../../build/css/components.css',
  'regions' => array(
    'top' => t('Top'),
    'first' => t('Left'),
    'second' => t('Right'),
    'bottom' => t('Bottom')
  ),
);
