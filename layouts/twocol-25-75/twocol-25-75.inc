<?php

// Plugin definition
$plugin = array(
  'title' => t('Two Column 25/75'),
  'category' => t('Ashlar'),
  'icon' => 'twocol-25-75.png',
  'theme' => 'twocol-25-75',
  'css' => '../../build/css/components.css',
  'regions' => array(
    'top' => t('Top'),
    'first' => t('Left'),
    'second' => t('Right'),
    'bottom' => t('Bottom')
  ),
);
