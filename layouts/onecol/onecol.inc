<?php

// Plugin definition
$plugin = array(
  'title' => t('One Column'),
  'category' => t('Ashlar'),
  'icon' => 'onecol.png',
  'theme' => 'onecol',
  'css' => '../../build/css/components.css',
  'regions' => array(
    'first' => t('Content Area')
  ),
);
