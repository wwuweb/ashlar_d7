<?php
/**
 * @file
 * Template for a layout with a single column
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['first']: Content
 */
?>
<div class="layout layout--onecol" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
	<div class="layout__region layout__region--content" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
		<?php print $content['first']; ?>
	</div>
</div>
