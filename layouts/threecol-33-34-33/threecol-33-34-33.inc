<?php

// Plugin definition
$plugin = array(
  'title' => t('Three Column 33/34/33'),
  'category' => t('Ashlar'),
  'icon' => 'threecol-33-34-33.png',
  'theme' => 'threecol-33-34-33',
  'css' => '../../build/css/components.css',
  'regions' => array(
    'top' => t('Top'),
    'first' => t('Left'),
    'second' => t('Middle'),
    'third' => t('Right'),
    'bottom' => t('Bottom')
  ),
);
