<?php

// Plugin definition
$plugin = array(
  'title' => t('Two Column 60/40'),
  'category' => t('Ashlar'),
  'icon' => 'twocol-66-34.png',
  'theme' => 'twocol-66-34',
  'css' => '../../build/css/components.css',
  'regions' => array(
    'top' => t('Top'),
    'first' => t('Left'),
    'second' => t('Right'),
    'bottom' => t('Bottom')
  ),
);
