# Western Washington University Drupal 7 Theme - Ashlar #
A Pseudo Theme for Drupal 7.

# IMPORTANT #
**Any theme styles or template development should not be happening in this repository.**

They should happen in the main [Ashlar Repository](https://bitbucket.org/wwuweb/ashlar/src). This theme is now very dependent on **Ashlar**. It has the necessary files included to allow it to run on a local Drupal 7 site, but update scripts regularly overwrite files in this repository.

The ONLY changes that need to happen here are:

* Adding a JavaScript file to the ashlar.info file (this file should already exist in the main Ashlar repository, only the reference to that file should be added here.)
* Adding Drupal Hooks to template.php (for specialized loading JavasSript files, mostly)
* If the template files in templates/other need to be updated (this should be very rare)

Do NOT attempt to update any of the css or .php.tpl files in the following directories:

* build/
* dist/
* templates/ashlar/

**Your changes will be overwritten** by the update scripts that keep this repository consistent with the main Ashlar repository.

All of the files that exist in these three directories come directly from **Ashlar**. Even if you are developing for a Drupal 7 site, you'll need to follow the instructions on the **Ashlar** readme to get a local Drupal 8 development environment set up.

To test changes locally on a Drupal 7 site, run `lando gulp` from the **ashlar** folder and replace all of the files in the build/ and dist/ in your local ashlar_d7 folder with those that get generated by the `lando gulp` command.

If you need to make changes to the templates/ashlar/page.tpl.php file, you'll need to make the changes in the `_patterns/03-templates/drupal7/drupal7.html.twig` file in **Ashlar**. You then generate a pattern lab instance, navigate to that template, and can copy and paste the resulting HTML into your local ashlar_d7 templates/ashlar/page.tpl.php file to test.

## Dependencies ##
  * [Zen 5.x](http://drupal.org/project/zen)
  * [Ultimenu](https://www.drupal.org/project/ultimenu)
  * Possibly some other things

This is an extension of the [Ashlar](https://bitbucket.org/wwuweb/ashlar/) Drupal 8 theme, made for Drupal 7.


## Current Use ##
The intention of this theme is to be consistent with what is output by Ashlar, while also being backwards compatible with the systems that already exist in Drupal 7 sites. While this theme does exist to support a few Drupal 7 sites that were created late in the Drupal 7 development cycle, our intention is to move towards migrating to sites to Drupal 8. This theme should only be used if moving to Drupal 8 is not feasible. Again, almost all development should be happening in [Ashlar](https://bitbucket.org/wwuweb/ashlar/src) and not here.

A list of sites that use this theme are:
* [Huxley](https://huxley.wwu.edu/)
* [Mabel](https://mabel.wwu.edu/)
* [Viking Union](https://vu.wwu.edu/)
* [Grad School](https://gradschool.wwu.edu/)
